// -------------------------------------------
// -------------- Pan Variables --------------
// -------------------------------------------

var curPos;
var cursorRadius;

var panEnabled = true;

var interpolationFactor = 1200;			//for atan division; in the default example was 660

var cursorAbs_VelocityFactor = {x:1.7, y:2.5};	//the higher it is, the higher is the cursor sensitivity
var cursorAbs_panVelocityFactor = 0.05;			//the higher it is, the fester is the map panning
var cursorAbs_firstPositionment = true;			//used to set correctly the cursor the first time
var cursorAbs_WiggleThreshold = 0.2;			//used to avoid cursor wiggle
var cursorAbs_safeMarginX = 100;				//safe margin for enabling pan on the x axis
var cursorAbs_safeMarginY = 60;					//safe margin for enabling pan on the y axis

var cursorDir_stabilizeFactor = 5;		//used in the interpolation
var cursorDir_multiplier = 100;			//used in the interpolation
var cursorDir_VelocityFactor = 1;		//the higher it is, the higher is the cursor sensitivity
var cursorDir_panVelocityFactor = 5;	//the higher it is, the faster is the map panning

var cursorVel_VelocityFactor = 8.5;		//the higher it is, the less is the cursor sensitivity
var cursorVel_panVelocityFactor = 5;	//the higher it is, the slower is the map panning
var cursorVel_wiggleThreshold = 10;		//threshold to avoid cursor wiggle

var handPos_panThresholdX = 45;			//threshold on the x axis
var handPos_panThresholdY = 25;			//threshold on the y axis
var handPos_safeMarginX = 30;			//safe margin for enabling pan on the x axis
var handPos_safeMarginY = 20;			//safe margin for enabling pan on the y axis
var handPos_panVelocityFactor = 15;		//the higher it is, the slower is the map panning
var handPos_panCenterY = 200;			//offset of the center on the y-axis
var handPos_threshMarginOfFrontmostHand = 140;	// used to pan even if 2 hands are detected

var handOri_hasToCalibrate = false;									//used to setup the "origin" of orientation
var handOri_panThreshold_width = 0.20;								//width of the "no_pan" area
var handOri_panThreshold_height = 0.40;								//height of the "no_pan" area
var handOri_panThresholdX = {left:0.5, right:0.2};					//thresholds on the x axis (can be calibrated)
var handOri_panThresholdY = {up:0.30, down:0};						//thresholds on the y axis (can be calibrated)
var handOri_panVelocityFactor = {left:25, right:25, ver:22};		//the higher it is, the slower is the map panning
var handOri_panCenterY = 200;										//offset of the center on the y-axis

var panHandDrag_previousPos = null;
var panHandDrag_maxVelocity = 60;
var panHandDrag_updatedVelocity = panHandDrag_maxVelocity;
var panHandDrag_finalDirection = null;
var panHandDrag_timerPan = null;

//	---------------------------------------------------------------------
//		Doc Ready
//	---------------------------------------------------------------------

$(document).ready(function() {
	curPos = {x:window.innerWidth/2, y:window.innerHeight/2}; // Initial position in the middle
	cursorRadius = $("#cursor").height()/2 +
		parseInt($("#cursor").css("border-width").charAt(0),10); //read from css
});



//	---------------------------------------------------------------------
//		Pan Functions
//	---------------------------------------------------------------------

function panCursorAbs (leap) {

	var frame = leap.frame();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array
	var iBox = frame.interactionBox;

	if (h.length >= 1 && f.length <=3 && f.length >= 1) {

		if (!zoomHandPinch_isZoomActive) {
			//Show cursor
			$("#cursor").show();
		}

		var finger = getFrontMostPointable( getPreferredHand(h).fingers );
		if (finger === null)
			finger = getFrontMostPointable(f);

		var abs_x = iBox.normalizePoint(finger.tipPosition)[0];
		var abs_y = 1 - iBox.normalizePoint(finger.tipPosition)[1];

		var transformed_x = 0.5 + cursorAbs_VelocityFactor.x*(abs_x-0.5);	//translate, multiply
		var transformed_y = 0.5 + cursorAbs_VelocityFactor.y*(abs_y-0.5);	//translate, multiply

		var newPos;
		var new_x = window.innerWidth  * transformed_x;
		var	new_y = window.innerHeight * transformed_y;

		if (cursorAbs_firstPositionment) {

			cursorAbs_firstPositionment = false;

			newPos = {
				x: new_x,
				y: new_y
			};

		}

		if (!cursorAbs_firstPositionment) {

			var diff_x = new_x - curPos.x;
			var diff_y = new_y - curPos.y;

			var interpolatedVelocity = stabilizeVelocity(
				diff_x,
				diff_y,
				1);

			if (Math.abs(interpolatedVelocity.x) < cursorAbs_WiggleThreshold)
				interpolatedVelocity.x = 0;
			if (Math.abs(interpolatedVelocity.y) < cursorAbs_WiggleThreshold)
				interpolatedVelocity.y = 0;

			newPos = {
				x: curPos.x + interpolatedVelocity.x,
				y: curPos.y + interpolatedVelocity.y
			};

		}

		// Used with ZOOM.FINGERS to prevent unvolountary panning after zooming
		if (panEnabled) {
			checkPosAndPan(newPos, cursorAbs_panVelocityFactor);
			
		} else {

			if (newPos.x >= window.innerWidth - cursorRadius)
				newPos.x = window.innerWidth - cursorRadius;
			else if(newPos.x <= cursorRadius )
				newPos.x = cursorRadius;

			if (newPos.y >= window.innerHeight-cursorRadius)
				newPos.y = window.innerHeight-cursorRadius;
			else if (newPos.y <= cursorRadius)
				newPos.y = cursorRadius;

			if (new_x >= cursorRadius+cursorAbs_safeMarginX &&
				new_x <= window.innerWidth-cursorRadius-cursorAbs_safeMarginX &&
				new_y >= cursorRadius+cursorAbs_safeMarginY &&
				new_y <= window.innerHeight-cursorRadius-cursorAbs_safeMarginY) {

				// cursor again in the "safe" area
				panEnabled = true;
			}
		}

		$("#cursor").css("left", newPos.x-cursorRadius)
			.css("top", newPos.y-cursorRadius);

		curPos = newPos;
		// searchForOrigin(curPos);

	} else {
		//Hide cursor
		$("#cursor").hide();
		panEnabled = false;
		hidePanControls();
	}
}

function panCursorDir (leap) {
	var frame = leap.frame();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array

	if (h.length == 1 && f.length <=3 && f.length >= 1) {

		//Show cursor
		$("#cursor").show();

		var finger = getFrontMostPointable(f);

		console.log(finger.direction);

		var interpolatedDirection = stabilizeVelocity(
			finger.direction[0]*cursorDir_multiplier,
			finger.direction[1]*cursorDir_multiplier,
			cursorDir_stabilizeFactor);

		var newPos = {
			x: cursorDir_VelocityFactor * window.innerWidth  *
				(interpolatedDirection.x + 0.5),
			y: cursorDir_VelocityFactor * window.innerHeight *
				(0.5-interpolatedDirection.y)
		};

		checkPosAndPan(newPos, cursorDir_panVelocityFactor);

		$("#cursor").css("left", newPos.x-cursorRadius)
			.css("top", newPos.y-cursorRadius);

		curPos = newPos;
		// searchForOrigin(curPos);

	} else {
		//Hide cursor
		$("#cursor").hide();
		hidePanControls();
	}
}

function panCursorVel (leap) {

	var frame = leap.frame();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array

	if (h.length == 1 && f.length <=3 && f.length >= 1) {

				$("#cursor").show();

				var finger = getFrontMostPointable(f);

				var interpolatedVelocity = stabilizeVelocity(
					finger.tipVelocity[0],
					finger.tipVelocity[1],
					cursorVel_VelocityFactor);

				var newPos = {
					x:(curPos.x + interpolatedVelocity.x),
					y:(curPos.y - interpolatedVelocity.y)
				};

				checkPosAndPan(newPos, cursorVel_panVelocityFactor);

				// if (Math.abs(curPos.x - newPos.x) < cursorVel_wiggleThreshold ||
				// Math.abs(curPos.y - newPos.y) < cursorVel_wiggleThreshold) {
					//	Move the cursor (div)
					$("#cursor").css("left", newPos.x-cursorRadius)
						.css("top", newPos.y-cursorRadius);

					curPos = newPos;
				// } else {
				// $("#cursor").hide();
				// }

			} else {
				$("#cursor").hide();
				//Hide cursor
				$("#cursor").hide();
				hidePanControls();
				// checkCursor(null);
			}
}

function panHandPos (leap) {
	var frame = leap.frame();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array


	if ((h.length == 1 || (h.length >= 2 && getFrontMostHand_Margin(h)>handPos_threshMarginOfFrontmostHand)) && f.length >=1) {

		var palm = getPreferredHand(h).palmPosition;

		// Used with ZOOM.FINGERS to prevent unvolountary panning after zooming
		if (panEnabled) {
			//Pan horizontally
			if (palm[0]>handPos_panThresholdX) {
				map.panBy((palm[0]-handPos_panThresholdX)/handPos_panVelocityFactor, 0);
			} else if (palm[0]<-handPos_panThresholdX) {
				map.panBy((palm[0]+handPos_panThresholdX)/handPos_panVelocityFactor, 0);
			}

			//Pan vertically
			if ((handPos_panCenterY-palm[1])>handPos_panThresholdY) {
				map.panBy(0, (handPos_panCenterY-palm[1]-handPos_panThresholdY)/handPos_panVelocityFactor);
			} else if ((handPos_panCenterY-palm[1])<-handPos_panThresholdY) {
				map.panBy(0, (handPos_panCenterY-palm[1]+handPos_panThresholdY)/handPos_panVelocityFactor);
			}

			//Display feedback
			if (palm[0]>handPos_panThresholdX) {
				displayArrow("Right",true);
				displayArrow("Left",false);
			} else if (palm[0]<-handPos_panThresholdX) {
				displayArrow("Left",true);
				displayArrow("Right",false);
			} else {
				displayArrow("Right",false);
				displayArrow("Left",false);
			}

			if ((handPos_panCenterY-palm[1])>handPos_panThresholdY) {
				displayArrow("Down",true);
				displayArrow("Up",false);
			} else if ((handPos_panCenterY-palm[1])<-handPos_panThresholdY) {
				displayArrow("Up",true);
				displayArrow("Down",false);
			} else {
				displayArrow("Up",false);
				displayArrow("Down",false);
			}
		} else {

			if (Math.abs(palm[0]) < handPos_panThresholdX+handPos_safeMarginX &&
				Math.abs(handPos_panCenterY-palm[1]) < handPos_panThresholdY+handPos_safeMarginY) {

				// cursor again in the "safe" area
				panEnabled = true;
			}
		}

	} else {
		panEnabled = false;
		hidePanControls();
	}
}

function panHandOri (leap) {
	var frame = leap.frame();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array

	if ((h.length == 1 || h.length == 2) && f.length >=1) {

		var hand = getRightMostHand(h);
		var direction = hand.direction;

		if (handOri_hasToCalibrate) {
			handOri_hasToCalibrate = false;

			calibrateHandOri(direction);
		}

		//Pan horizontally
		if (direction[0] > handOri_panThresholdX.right) {
			map.panBy( (direction[0]-handOri_panThresholdX.right) *
				handOri_panVelocityFactor.right, 0);
			displayArrow("Right",true);
			displayArrow("Left",false);
		} else if (direction[0] < -handOri_panThresholdX.left) {
			map.panBy( (direction[0]+handOri_panThresholdX.left) *
				handOri_panVelocityFactor.left, 0);
			displayArrow("Left",true);
			displayArrow("Right",false);
		} else {
			displayArrow("Right",false);
			displayArrow("Left",false);
		}


		// //Pan vertically
		if (direction[1] > handOri_panThresholdY.up) {
			map.panBy(0, -(direction[1]-handOri_panThresholdY.up) *
				handOri_panVelocityFactor.ver);
			displayArrow("Down",false);
			displayArrow("Up",true);
		} else if (direction[1] < -handOri_panThresholdY.down) {
			map.panBy(0, -(direction[1]+handOri_panThresholdY.down) *
				handOri_panVelocityFactor.ver);
			displayArrow("Up",false);
			displayArrow("Down",true);
		} else {
			displayArrow("Up",false);
			displayArrow("Down",false);
		}

	} else
		hidePanControls();
}

function panHandDrag (leap) {
	var frame = leap.frame();

	var h = frame.hands; //hands array

	//DETECT GESTURES
	if (frame.gestures.length > 0) {
		frame.gestures.forEach(function(gesture) {

			//Circle gesture zooms in&out the map
			if (gesture.type == "swipe") {
				console.log(gesture.id);
			}
		});
	}

	if (h.length >= 1 && getPreferredHand(h).fingers.length >= 1) {
		var f = getFrontMostPointable(getPreferredHand(h).fingers);
		var p = getPreferredHand(h);

		if (f.tipPosition[2] < -100) {

			if (panHandDrag_previousPos !== null) {

				panHandDrag_updatedVelocity = panHandDrag_maxVelocity;

				panHandDrag_finalDirection = [
					(Math.log(1+Math.abs(p.palmVelocity[0]/20)))*3*(panHandDrag_previousPos[0]-p.palmPosition[0]),
					(Math.log(1+Math.abs(p.palmVelocity[1]/20)))*3*(panHandDrag_previousPos[1]-p.palmPosition[1])
				];

				map.panBy(-panHandDrag_finalDirection[0], panHandDrag_finalDirection[1]);
			}

			panHandDrag_previousPos = p.palmPosition;

		} else {
			panHandDrag_stopDragging();
		}

		$("#screen_dim").addClass("dragFocus").show();

		var o;

		if (f.tipPosition[2] < -100)
			o = 1;
		else if (f.tipPosition[2] > 0)
			o = 0;
		else
			o = -(f.tipPosition[2] / 100)/2;

		$("#screen_dim").css("opacity", o);

	} else {
		panHandDrag_stopDragging();
		$("#screen_dim").removeClass("dragFocus").css("opacity", "").hide();
	}
}


//	---------------------------------------------------------------------
//		Utility Functions
//	---------------------------------------------------------------------

function stabilizeVelocity(x,y,velocityFactor) {

	if (currentPan == PAN.CURSOR_ABS) {
		var t = 300;
		x = x > t ? t : x;
		x = x < -t ? -t : x;
		y = y > t ? t : y;
		y = y < -t ? -t : y;
	}

	var magnitude = Math.sqrt((x*x)+(y*y));
	var gain = 1;


	gain = Math.atan(magnitude/interpolationFactor) / 0.5; //default: 660

	toReturn = {
		x: gain*x/velocityFactor,
		y: gain*y/velocityFactor
	};

	return toReturn;
}

function checkPosAndPan (newPos, velocityFactor) {
	var maxPan = 10;

	var pan = 0;

	if (newPos.x >= window.innerWidth - cursorRadius) {
		pan = velocityFactor * (newPos.x - (window.innerWidth - cursorRadius));
		pan = pan>maxPan ? maxPan : pan;

		map.panBy(pan,0);
		newPos.x = window.innerWidth - cursorRadius;
		displayArrow("Right",true);
		displayArrow("Left",false);
	} else if(newPos.x <= cursorRadius ){
		pan = velocityFactor * (newPos.x - cursorRadius);
		pan = pan<-maxPan ? -maxPan : pan;

		map.panBy(pan,0);
		newPos.x = cursorRadius;
		displayArrow("Left",true);
		displayArrow("Right",false);
	} else {
		displayArrow("Right",false);
		displayArrow("Left",false);
	}

	if (newPos.y >= window.innerHeight-cursorRadius) {
		pan = velocityFactor * (newPos.y - (window.innerHeight-cursorRadius));
		pan = pan>maxPan ? maxPan : pan;

		map.panBy(0,pan);
		newPos.y = window.innerHeight-cursorRadius;
		displayArrow("Up",false);
		displayArrow("Down",true);
	} else if(newPos.y <= cursorRadius ){
		pan = velocityFactor * (newPos.y - cursorRadius);
		pan = pan<-maxPan ? -maxPan : pan;

		map.panBy(0,pan);
		newPos.y = cursorRadius;
		displayArrow("Up",true);
		displayArrow("Down",false);
	} else {
		displayArrow("Up",false);
		displayArrow("Down",false);
	}
}

function hidePanControls () {
	displayArrow("Left",false);
	displayArrow("Right",false);
	displayArrow("Up",false);
	displayArrow("Down",false);
}

function calibrateHandOri (direction) {


	handOri_panThresholdX = {
		left:Math.abs(direction[0]-handOri_panThreshold_width/2),
		right:Math.abs(direction[0]+handOri_panThreshold_width/2)
	};

	handOri_panThresholdY = {
		up:Math.abs(direction[1]+handOri_panThreshold_height/2),
		down:Math.abs(direction[1]-handOri_panThreshold_height/2)
	};

	console.log(handOri_panThresholdY);
}

function panHandDrag_stopDragging () {
	if (panHandDrag_previousPos !== null) {
		panHandDrag_previousPos = null;

		// panHandDrag_timerPan = setInterval(
		// 	function() {
		// 		var ratio = panHandDrag_maxVelocity / panHandDrag_updatedVelocity;

		// 		map.panBy(
		// 			panHandDrag_finalDirection[0] / ratio,
		// 			panHandDrag_finalDirection[1] / ratio
		// 		);

		// 		panHandDrag_updatedVelocity -= 1;

		// 		if (panHandDrag_updatedVelocity <= 0) {
		// 				console.log("hello");
		// 			clearInterval(panHandDrag_timerPan);
		// 		}
		// 	},
		// 	1000/30
		// );
	}
}