//	---------------------------------------------------------------------
//		Set Position of UI elements
//	---------------------------------------------------------------------


function setElementsAbsolutePosition() {

	totWidth= $("#mapContainer").width();
	totHeight= $("#mapContainer").height();

	$("#arrowLeft, #arrowRight").css("top", (totHeight-$("#arrowLeft").height())/2+"px");
	$("#arrowUp, #arrowDown").css("left", (totWidth-$("#arrowUp").width())/2+"px");

	$("#zoomIn, #zoomOut, #zoomLimit")
		.css("left", (totWidth-$("#zoomIn").width())/2+"px");

	$("#target")
		.css("top", (totHeight-$("#target").height())/2+"px")
		.css("left", (totWidth-$("#target").width())/2+"px");
}

function displayArrow(string_Direction, flag_display) {
	if (flag_display) {
		$("#arrow"+string_Direction).removeClass("arrowDefault");
		$("#arrow"+string_Direction).addClass("arrowHover");
	} else {
		$("#arrow"+string_Direction).removeClass("arrowHover");
		$("#arrow"+string_Direction).addClass("arrowDefault");
	}
}



//	---------------------------------------------------------------------
//		Full Screen HTML5 APIs
//	---------------------------------------------------------------------

function toggleFullscreen () {
	var fullScreen = document.fullscreenEnabled || document.mozFullscreenEnabled || document.webkitIsFullScreen ? true : false;

	if (fullScreen)
		exitFullscreen();
	else
		enterFullscreen(document.body);
}

function enterFullscreen(element) {
    if(element.requestFullScreen) {
		//fonction officielle du w3c
		element.requestFullScreen();
    } else if(element.webkitRequestFullScreen) {
		//fonction pour Google Chrome (on lui passe un argument pour autoriser le plein écran lors d'une pression sur le clavier)
		element.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
    } else if(element.mozRequestFullScreen){
		//fonction pour Firefox
		element.mozRequestFullScreen();
    } else {
		alert('Votre navigateur ne supporte pas le mode plein écran, il est temps de passer à un plus récent ;)');
    }
}

function exitFullscreen() {
	if(document.cancelFullScreen) {
		//fonction officielle du w3c
		document.cancelFullScreen();
	} else if(document.webkitCancelFullScreen) {
		//fonction pour Google Chrome
		document.webkitCancelFullScreen();
	} else if(document.mozCancelFullScreen){
		//fonction pour Firefox
		document.mozCancelFullScreen();
	}
}