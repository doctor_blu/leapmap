var isLeftHanded = false;

//	---------------------------------------------------------------------
//		Pan & Zoom Levels
//	---------------------------------------------------------------------


var PAN = {
	CURSOR_ABS: {val:1, string:"Cursor Abs"},
	CURSOR_VEL: {val:2, string:"Cursor Vel"},
	HAND_POS:	{val:3, string:"Hand Pos"},
	HAND_ORI:	{val:4, string:"Hand Ori"},
	CURSOR_ORI: {val:5, string:"Cursor Ori"}
};
var ZOOM = {
	CIRCLE:		{val:-1, string:"Circle"},
	HAND_POS:	{val:-2, string:"Hand Pos"},
	FINGERS:	{val:-3, string:"Hand Pinch"}
};

var currentPan = PAN.HAND_POS;
var	currentZoom = ZOOM.FINGERS;

$().ready(function() {

	// -------------------------------------------
	// -------------- KeyCodes -------------------
	// -------------------------------------------
	var keyCodes = {
		one:		49,		// Pan: cursor with iBox
		two:		50,		// Pan: hand orientation
		three:		51,		// Pan: hand 3d position (x,y axis)
		four:		52,		// Pan: cursor with velocity
		five:		53,		// Pan: cursor orientation
		q:			113,	// Zoom: circle gesture (clockwise and counter-clockwise)
		w:			119,	// Zoom: hand 3d position (z axis)
		e:			101,	// Zoom: finger pinching
		space:		32,		// Toggle leap interaction on/off
		c:			99,		// Calibrate hand orientation
		d:			100		// Calibrate hand depth center

	};


	// -------------------------------------------
	// -------------- KeyPress -------------------
	// -------------------------------------------
	$("body").keypress(function(event) {
		$("#cursor").hide();
		cursorAbs_firstPositionment = true;

		// console.log(event.keyCode);

		switch(event.keyCode) {
			case keyCodes.one:   currentPan =	PAN.CURSOR_ABS;
				break;
			case keyCodes.two:   currentPan =	PAN.HAND_ORI;
				break;
			case keyCodes.three: currentPan =	PAN.HAND_POS;
				break;
			// case keyCodes.four:  currentPan =	PAN.CURSOR_VEL;
			// 	break;
			// case keyCodes.five:  currentPan =	PAN.CURSOR_ORI;
			// 	break;
			case keyCodes.q:    currentZoom =	ZOOM.CIRCLE;
				break;
			case keyCodes.w:    currentZoom =	ZOOM.HAND_POS;
				break;
			case keyCodes.e:    currentZoom =	ZOOM.FINGERS;
				break;
			case keyCodes.space: ignoreLeap =	!ignoreLeap; $("#screen_dim").fadeToggle();
				break;
			case keyCodes.c: handOri_hasToCalibrate = true;
				break;
			case keyCodes.d: palmDepth_hasToCalibrate = true;
				break;
			default:
				//nothing
		}

		$("#info_pan>span").html(currentPan.string);
		$("#info_zoom>span").html(currentZoom.string);
		console.log(currentPan.string + " // " + currentZoom.string);
	});

	// -------------------------------------------
	// -------------- WinResize ------------------
	// -------------------------------------------
	setElementsAbsolutePosition();

	$(window).resize(function() {
		setElementsAbsolutePosition();
	});

	// -------------------------------------------
	// -------------- Leap Motion ----------------
	// -------------------------------------------

	var controller = new Leap.Controller({ enableGestures: true });

	// -------------------------------------------
	// -------------- MAIN LOOP ------------------
	// -------------------------------------------
	controller.loop(function(frame) {

		// searchForUnlockGesture(this);

		if (!ignoreLeap) {

			$("#tracking").slideDown();
			$("#info_hands>span").html(frame.hands.length+" x");
			$("#info_fingers>span").html(frame.pointables.length+" x");

			switch(currentPan) {
				case PAN.CURSOR_ABS:	panCursorAbs(this);
					break;
				case PAN.CURSOR_VEL:	panCursorVel(this);
					break;
				case PAN.HAND_POS:		panHandPos(this);
					break;
				case PAN.HAND_ORI:		panHandDrag(this);
					break;
				case PAN.CURSOR_ORI:	panCursorDir(this);
					break;
				default:
					//nothing
			}

			switch(currentZoom) {
				case ZOOM.CIRCLE:		zoomCircle(this);
					break;
				case ZOOM.HAND_POS:		zoomHandPos(this);
					break;
				case ZOOM.FINGERS:		zoomHandPinching(this);
					break;
				default:
					//nothing
			}

		} else {
			$("#tracking").slideUp();
			$("#info_pan>span").html("disabled");
			$("#info_zoom>span").html("disabled");
		}

	});

});


function getFrontMostPointable(pointArray) {

	if (pointArray.length === 0)
		return null;

	var pointToReturn = pointArray[0];

	for (var j=0; j<pointArray.length; j++)
		if (pointArray[j].tipPosition[2]<pointToReturn.tipPosition[2])
			pointToReturn = pointArray[j];

	return pointToReturn;
}

function getRightMostHand(hands) {

	hands.sort(function(a,b) {
		return b.palmPosition[0]-a.palmPosition[0];
	});

	return hands[0];
}

function getFrontMostHand_Margin(hands) {

	if (hands.length < 2)
		return 0;
	else {
		hands.sort(function(a,b) {
			return b.palmPosition[2]-a.palmPosition[2];
		});
		return hands[0].palmPosition[2]-hands[1].palmPosition[2];
	}
}

function getLeftMostHand(hands) {

	hands.sort(function(a,b) {
		return a.palmPosition[0]-b.palmPosition[0];
	});

	return hands[0];
}

function getPreferredHand (hands) {
	if (isLeftHanded)
		return getLeftMostHand(hands);
	else
		return getRightMostHand(hands);
}

function angleBetweenVectors(a, b) {
	var dotProd =			a[0]*b[0] + a[1]*b[1] + a[2]*b[2]	;
	var maga = Math.sqrt(	a[0]*a[0] + a[1]*a[1] + a[2]*a[2]	);
	var magb = Math.sqrt(	b[0]*b[0] + b[1]*b[1] + b[2]*b[2])	;

	return Math.acos(dotProd / (maga * magb));
}