$().ready(function() {
	
	var scale = 1.0;
	
	$("body").keypress(function(evt) {


	
		if (evt.keyCode == 119) {
			
			$("#map-canvas-copy").show();
			
			if (scale>=1)
				scale = Math.floor(10*scale+1)/10;
			else
				scale = Math.floor(100*scale+5)/100;
				
			console.log(scale);
			
			if (scale == 1.6 || scale == 0.8) {
				map.setZoom(map.getZoom()+1);
				console.log("map has precharged new zoom")
			}
			
			if (scale < 2)
				$("#map-canvas-copy").css("-webkit-transform", "scale("+scale+")");
			else {
				$("#map-canvas-copy").hide();
				scale = 1.0;
				$("#map-canvas-copy").css("-webkit-transform", "scale("+scale+")");
				mapCopy.setZoom(mapCopy.getZoom()+1);
			}
		}
		
		if (evt.keyCode == 115) {
			
			$("#map-canvas-copy").show();
			
			if (scale>1)
				scale = Math.floor(10*scale-1)/10;
			else
				scale = Math.floor(100*scale-5)/100;
				
			console.log(scale);
			
			if (scale == 1.5 || scale == 0.75) {
				map.setZoom(map.getZoom()-1);
				console.log("map has precharged new zoom")
			}
			
			if (scale > 0.5)
				$("#map-canvas-copy").css("-webkit-transform", "scale("+scale+")");
			else {
				$("#map-canvas-copy").hide();
				scale = 1.0;
				$("#map-canvas-copy").css("-webkit-transform", "scale("+scale+")");
				mapCopy.setZoom(mapCopy.getZoom()-1);
			}
		}
	});
});