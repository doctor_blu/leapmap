//	---------------------------------------------------------------------
//		Zoom Vars
//	---------------------------------------------------------------------


var mapZoomLevel = 0;				//keep track of the current zoom
var zoomInTimer;					//timer interval that zooms in every (param) ms
var zoomOutTimer;					//timer interval that zooms out every (param) ms
var timerIsRunning = false;			//flag to control the timers
var zoomInThreshold = 12;			// max zoom in value for maps
var zoomOutThreshold = 6;			// max zoom out value for maps


var zoomCircle_lastCircleId = 0;			//used to understand which circle gesture is currently done
var zoomCircle_clockwise = 0;				//can be set to -1 or +1
var zoomCircle_progressThreshold = 1;		//progress of the circle gesture before that the zoom is activated
var zoomCircle_zoomVelocityMs = 300;		//min progress value that let the gesture do switch
var zoomCircle_center = [];					//center of the circle, where the zoom will center
var zoomCircle_firstPan = false;


var palmDepth_hasToCalibrate = false;	//calibration for the depth center
var palmDepth_Threshold = 60;			//threshold on the z-axis
var palmDepth_Center = 0;				//offset of the center on the z-axis
var palmDepth_zoomVelocityMs = 400;		//time interval between every zoom in the timer


var zoomFingers_f_left;										//frontmost finger of the left hand
var zoomFingers_f_right;									//frontmost finger of the right hand
var zoomFingers_cumulative = -1;							//cumulative sum of diff between fingers
var zoomFingers_cumulativeThreshold = 15000;				//threshold for triggering next zoom level
var zoomFingers_dist = 0;									//distance between fingers
var zoomFingers_searchFingersFlag = true;					//true -> search for fingers; false -> zoom
var zoomFingers_framesPassed = 0;							//frames passed with 2 fingers still
var zoomFingers_framesPassedThreshold = 10;					//threshold for activating the zoom mode
var zoomFingers_enableZoomIn = true;						//flag to disable/enable zoom in
var zoomFingers_enableZoomOut = true;						//flag to disable/enable zoom out
var zoomFingers_consecutive_invalid_frames = 0;				//consecutive invalid frames passed
var zoomFingers_consecutive_invalid_frames_thresh = 20;		//threshold to deactivare the zoom after invalid frames
var zoomFingers_zoomWasEverValid = false;
var zoomFingers_zThreshold = 80;


var zoomHandPinch_h_left;										//frontmost finger of the left hand
var zoomHandPinch_h_right;									//frontmost finger of the right hand
var zoomHandPinch_cumulative = -1;							//cumulative sum of diff between fingers
var zoomHandPinch_cumulativeThreshold = 15000;				//threshold for triggering next zoom level
var zoomHandPinch_dist = 0;									//distance between fingers
var zoomHandPinch_searchFingersFlag = true;					//true -> search for fingers; false -> zoom
var zoomHandPinch_framesPassed = 0;							//frames passed with 2 fingers still
var zoomHandPinch_framesPassedThreshold = 5;					//threshold for activating the zoom mode
var zoomHandPinch_enableZoomIn = true;						//flag to disable/enable zoom in
var zoomHandPinch_enableZoomOut = true;						//flag to disable/enable zoom out
var zoomHandPinch_consecutive_invalid_frames = 0;				//consecutive invalid frames passed
var zoomHandPinch_consecutive_invalid_frames_thresh = 40;		//threshold to deactivare the zoom after invalid frames
var zoomHandPinch_zoomWasEverValid = false;
var zoomHandPinch_zThreshold = 80;
var zoomHandPinch_isZoomActive = false;

//	---------------------------------------------------------------------
//		Zoom Functions
//	---------------------------------------------------------------------

function zoomCircle (leap) {
	var frame = leap.frame();
	if (currentPan == PAN.CURSOR_ABS)
		$("#target").hide();
	else
		$("#target").show();

	//DETECT GESTURES
	if (frame.gestures.length > 0) {
		frame.gestures.forEach(function(gesture) {

			//Circle gesture zooms in&out the map
			if (gesture.type == "circle") {

				if (gesture.state == "stop")
					stopTimers();
				else {

					if (gesture.id > zoomCircle_lastCircleId) {
						// new gesture detected
						zoomCircle_lastCircleId = gesture.id;
						zoomCircle_clockwise = 0;
						zoomCircle_circleGestureHasSwitched = false;

					} else {
						// same gesture keeps going

						if (gesture.progress >= zoomCircle_progressThreshold &&
							!zoomCircle_circleGestureHasSwitched) {

							/* Utility code for detect clockwise-counterclockwise*/
							var direction = frame.finger(gesture.pointableIds[0]).direction;
							var norm = gesture.normal;

							zoomCircle_center[0] = window.innerWidth;
							zoomCircle_center[1] = window.innerHeight;

							if (zoomCircle_clockwise === 0)
								if (angleBetweenVectors(direction, norm) <= Math.PI / 2)
									zoomCircle_clockwise = 1;
								else if (angleBetweenVectors(direction, norm) > Math.PI / 2)
									zoomCircle_clockwise = -1;

							if (zoomCircle_clockwise === 1) {
								//zoom in
								if (!timerIsRunning) {

									if (map.getZoom() == zoomInThreshold) {
										$("#zoomLimit").show();
									} else {

										if (currentPan == PAN.CURSOR_ABS) {
											var iBox1 = frame.interactionBox;
											zoomCircle_center[0] = (((iBox1.normalizePoint(gesture.center)[0] - 0.5) *cursorAbs_VelocityFactor.x) + 0.5) *window.innerWidth;
											zoomCircle_center[1] = ((((1-iBox1.normalizePoint(gesture.center)[1]) - 0.5) *cursorAbs_VelocityFactor.y) + 0.5) *window.innerHeight;
											zoomCircle_firstPan = true;
										}

										zoomInTimer = setInterval(function() {
											mapZoomLevel = map.getZoom()+1;
											if (mapZoomLevel > zoomInThreshold) {
												mapZoomLevel = zoomInThreshold;
												$("#zoomIn").hide();
												$("#zoomLimit").show();
											} else
												$("#zoomIn").show();

											if (zoomCircle_firstPan) {
												map.panBy(
													zoomCircle_center[0] - window.innerWidth/2,
													zoomCircle_center[1] - window.innerHeight/2);
												zoomCircle_firstPan = false;
											}

											map.setZoom(mapZoomLevel);

										}, zoomCircle_zoomVelocityMs);

										timerIsRunning = true;
										zoomCircle_circleGestureHasSwitched = true;
									}
								}
							} else if (zoomCircle_clockwise === -1) {
								//zoom out
								if (!timerIsRunning) {

									if (map.getZoom() == zoomOutThreshold) {
										$("#zoomLimit").show();
									} else {

										if (currentPan == PAN.CURSOR_ABS) {
											var iBox2 = frame.interactionBox;
											zoomCircle_center[0] = (((iBox2.normalizePoint(gesture.center)[0] - 0.5) *cursorAbs_VelocityFactor.x) + 0.5) *window.innerWidth;
											zoomCircle_center[1] = ((((1-iBox2.normalizePoint(gesture.center)[1]) - 0.5) *cursorAbs_VelocityFactor.y) + 0.5) *window.innerHeight;
											zoomCircle_firstPan = true;
										}

										zoomOutTimer = setInterval(function() {
											mapZoomLevel = map.getZoom()-1;
											if (mapZoomLevel < zoomOutThreshold) {
												mapZoomLevel = zoomOutThreshold;
												$("#zoomOut").hide();
												$("#zoomLimit").show();
											} else
												$("#zoomOut").show();

											if (zoomCircle_firstPan) {
												map.panBy(
													zoomCircle_center[0] - window.innerWidth/2,
													zoomCircle_center[1] - window.innerHeight/2);
												zoomCircle_firstPan = false;
											}

											map.setZoom(mapZoomLevel);
										}, zoomCircle_zoomVelocityMs);

										timerIsRunning = true;
										zoomCircle_circleGestureHasSwitched = true;
									}
								}
							}
						}
					}
				}
			}
		});
	} else
		stopTimers();
}

function zoomHandPos (leap) {
	var frame = leap.frame();
	$("#target").show();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array

	if ((h.length == 1 || h.length == 2) && f.length >=0) {

		var palm = getRightMostHand(h).palmPosition;

		if (palmDepth_hasToCalibrate) {
			palmDepth_hasToCalibrate = false;
			palmDepth_Center = palm[2];
		}

		if (palm[2] > (palmDepth_Center+palmDepth_Threshold)) {

			if (!timerIsRunning) {
				if (map.getZoom() == zoomOutThreshold) {
					$("#zoomLimit").show();
				} else {
					zoomOutTimer = setInterval(function() {
						mapZoomLevel = map.getZoom()-1;
						if (mapZoomLevel < zoomOutThreshold) {
							mapZoomLevel = zoomOutThreshold;
							$("#zoomOut").hide();
							$("#zoomLimit").show();
						} else
							$("#zoomOut").show();

						map.setZoom(mapZoomLevel);
					}, palmDepth_zoomVelocityMs);
					timerIsRunning = true;
				}
			}

		} else if (palm[2] < (palmDepth_Center-palmDepth_Threshold)) {

			//zoom in timer
			if (!timerIsRunning) {

				if (map.getZoom() == zoomInThreshold) {
					$("#zoomLimit").show();
				} else {
					zoomInTimer = setInterval(function() {
						mapZoomLevel = map.getZoom()+1;
						if (mapZoomLevel > zoomInThreshold) {
							mapZoomLevel = zoomInThreshold;
							$("#zoomIn").hide();
							$("#zoomLimit").show();
						} else
							$("#zoomIn").show();

						map.setZoom(mapZoomLevel);
					}, palmDepth_zoomVelocityMs);
					timerIsRunning = true;
				}

			}
		} else {
			//hand in the middle, stop timers
			stopTimers();
		}
	} else
		//not enough hands/fingers are seen
		stopTimers();
}

function zoomFingers (leap) {

	var frame = leap.frame();
	$("#target").show();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array

	var isInvalidFrame = false;
	var isZoomActive = false;

	var check = checkZooomFingers(h);

	if (check.valid) {

		// get the two pinching fingers
		zoomFingers_f_left = getFrontMostPointable(check.left.fingers);
		zoomFingers_f_right = getFrontMostPointable(check.right.fingers);

		if (Math.abs(zoomFingers_f_left.tipPosition[2] - zoomFingers_f_right.tipPosition[2])>zoomFingers_zThreshold) {
			zoomFingers_consecutive_invalid_frames++;
			isInvalidFrame = true;
		} else {
			zoomFingers_consecutive_invalid_frames = 0;
			zoomFingers_zoomWasEverValid = true;
			isZoomActive = true;
		}

	} else {
		zoomFingers_consecutive_invalid_frames++;
		isInvalidFrame = true;
	}

	if (zoomFingers_consecutive_invalid_frames == zoomFingers_consecutive_invalid_frames_thresh) {
		isZoomActive = false;
		if (panEnabled && zoomFingers_zoomWasEverValid) {
			panEnabled = false;
		}
		zoomFingers_zoomWasEverValid = false;
	}

	//two hands, with at least one finger each
	if (isZoomActive) {


		// search for fingers that don't move for a certain time
		if (zoomFingers_searchFingersFlag) {

			$("#zoomBar").hide();

			if (!isInvalidFrame && Math.abs(zoomFingers_dist - (zoomFingers_f_left.tipPosition[0] - zoomFingers_f_right.tipPosition[0])) < 1) {
				zoomFingers_framesPassed++;

				// after 30 frames in which the fingers stay still, we activate the zoom mode
				if (zoomFingers_framesPassed >= zoomFingers_framesPassedThreshold) {
					zoomFingers_framesPassed = 0;
					zoomFingers_searchFingersFlag = false;
				}

			} else {
				// the fingers moved to much, start tracking again
				zoomFingers_framesPassed = 0;
			}

		// enter zoom mode (not searching anymore the fingers)
		} else {

			if (zoomFingers_cumulative == -1) {
				// first time in the loop
				zoomFingers_cumulative = 0;
				zoomFingers_framesPassed = 0;
				$("#zoomBar").show();
			} else {
				zoomFingers_cumulative += Math.abs(zoomFingers_f_left.tipVelocity[0] - zoomFingers_f_right.tipVelocity[0]) * (zoomFingers_dist - (zoomFingers_f_left.tipPosition[0] - zoomFingers_f_right.tipPosition[0]));
			}

			// Look at the value of zoomFingers_cumulative and eventually zoomIn or zoomOut
			if (zoomFingers_cumulative > zoomFingers_cumulativeThreshold && zoomFingers_enableZoomIn) {

				zoomFingers_cumulative = 0;
				$("#zoomOut").hide();

				if (map.getZoom() < zoomInThreshold) {
					map.setZoom(map.getZoom()+1);
					$("#zoomLimit").hide();
					$("#zoomIn").show();
				} else {
					$("#zoomIn").hide();
					$("#zoomLimit").show();
				}

				// used in order to be able to zoom only in one direction
				// zoomFingers_enableZoomOut = false;
			} else if (zoomFingers_cumulative < -zoomFingers_cumulativeThreshold && zoomFingers_enableZoomOut) {

				zoomFingers_cumulative = 0;
				$("#zoomIn").hide();

				if (map.getZoom() > zoomOutThreshold) {
					map.setZoom(map.getZoom()-1);
					$("#zoomLimit").hide();
					$("#zoomOut").show();
				} else {
					$("#zoomOut").hide();
					$("#zoomLimit").show();
				}

				// used in order to be able to zoom only in one direction
				// zoomFingers_enableZoomIn = false;
			}

			drawZoombar(zoomFingers_cumulative, zoomFingers_cumulativeThreshold);

			// used to exit from zoom mode and come back in search fingers mode
			// zoomFingers_searchFingersFlag = true;
		}

		zoomFingers_dist = zoomFingers_f_left.tipPosition[0] - zoomFingers_f_right.tipPosition[0];

	} else {
		// there are not two hands with at least one finger each
		resetFingerZoom();
	}
}

function zoomHandPinching (leap) {

	var frame = leap.frame();
	$("#target").show();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array

	var isInvalidFrame = false;

	var check = checkHandsForPinching(h, f);

	if (check.valid) {

		// get the two pinching fingers
		zoomHandPinch_h_left = check.left;
		zoomHandPinch_h_right = check.right;

		if (Math.abs(zoomHandPinch_h_left.palmPosition[2] - zoomHandPinch_h_right.palmPosition[2])>zoomHandPinch_zThreshold) {
			zoomHandPinch_consecutive_invalid_frames++;
			isInvalidFrame = true;
		} else {
			zoomHandPinch_consecutive_invalid_frames = 0;
			zoomHandPinch_zoomWasEverValid = true;
			zoomHandPinch_isZoomActive = true;
		}

	} else {
		zoomHandPinch_consecutive_invalid_frames++;
		isInvalidFrame = true;
	}

	if (zoomHandPinch_consecutive_invalid_frames == zoomHandPinch_consecutive_invalid_frames_thresh) {
		zoomHandPinch_isZoomActive = false;
		if (panEnabled && zoomHandPinch_zoomWasEverValid) {
			panEnabled = false;
		}
		zoomHandPinch_zoomWasEverValid = false;
	}

	//two hands, with at least one finger each
	if (zoomHandPinch_isZoomActive) {


		// search for fingers that don't move for a certain time
		if (zoomHandPinch_searchFingersFlag) {

			$("#zoomBar").hide();

			if (!isInvalidFrame && Math.abs(zoomHandPinch_dist - (zoomHandPinch_h_left.palmPosition[0] - zoomHandPinch_h_right.palmPosition[0])) < 1.5) {
				zoomHandPinch_framesPassed++;

				// after 30 frames in which the fingers stay still, we activate the zoom mode
				if (zoomHandPinch_framesPassed >= zoomHandPinch_framesPassedThreshold) {
					zoomHandPinch_framesPassed = 0;
					zoomHandPinch_searchFingersFlag = false;
				}

			} else {
				// the fingers moved to much, start tracking again
				zoomHandPinch_framesPassed = 0;
			}

		// enter zoom mode (not searching anymore the fingers)
		} else {

			if (zoomHandPinch_cumulative == -1) {
				// first time in the loop
				zoomHandPinch_cumulative = 0;
				zoomHandPinch_framesPassed = 0;
				$("#zoomBar").show();

				//if we can only zoom in one direction, prevent the other one
				// if (map.getZoom() == zoomInThreshold)
				// zoomHandPinch_enableZoomIn = false;
				// if (map.getZoom() == zoomOutThreshold)
				// 	zoomHandPinch_enableZoomOut = false;
			} else {
				zoomHandPinch_cumulative += 40*Math.log(Math.abs(zoomHandPinch_h_left.palmVelocity[0] - zoomHandPinch_h_right.palmVelocity[0])) * (zoomHandPinch_dist - (zoomHandPinch_h_left.palmPosition[0] - zoomHandPinch_h_right.palmPosition[0]));
			}

			// Look at the value of zoomHandPinch_cumulative and eventually zoomIn or zoomOut
			if (zoomHandPinch_cumulative > zoomHandPinch_cumulativeThreshold && zoomHandPinch_enableZoomIn) {

				zoomHandPinch_cumulative = 0;
				$("#zoomOut").hide();

				if (map.getZoom() < zoomInThreshold) {
					map.setZoom(map.getZoom()+1);
					$("#zoomLimit").hide();
					$("#zoomIn").show();
				} else {
					$("#zoomIn").hide();
					$("#zoomLimit").show();
				}

				// used in order to be able to zoom only in one direction
				// zoomHandPinch_enableZoomOut = false;
			} else if (zoomHandPinch_cumulative < -zoomHandPinch_cumulativeThreshold && zoomHandPinch_enableZoomOut) {

				zoomHandPinch_cumulative = 0;
				$("#zoomIn").hide();

				if (map.getZoom() > zoomOutThreshold) {
					map.setZoom(map.getZoom()-1);
					$("#zoomLimit").hide();
					$("#zoomOut").show();
				} else {
					$("#zoomOut").hide();
					$("#zoomLimit").show();
				}

				// used in order to be able to zoom only in one direction
				// zoomHandPinch_enableZoomIn = false;
			}

			drawZoombar(zoomHandPinch_cumulative, zoomHandPinch_cumulativeThreshold);

			// used to exit from zoom mode and come back in search fingers mode
			// zoomHandPinch_searchFingersFlag = true;
		}

		zoomHandPinch_dist = zoomHandPinch_h_left.palmPosition[0] - zoomHandPinch_h_right.palmPosition[0];

	} else {
		// there are not two hands with at least one finger each
		resetHandsPinchZoom();
	}
}


//	---------------------------------------------------------------------
//		Utility Functions
//	---------------------------------------------------------------------

function stopTimers() {
	clearInterval(zoomInTimer);
	clearInterval(zoomOutTimer);
	timerIsRunning = false;
	$("#zoomIn, #zoomOut, #zoomLimit").hide();
}

function drawZoombar(cumulative, maxCumulative) {
	var range_size = zoomInThreshold - zoomOutThreshold;
	var z = map.getZoom() - range_size; //zoom ranges between 6 and 12, z ranges between 0 and 6
	var single_zoom_width = $("#zoomBar").width() / range_size;
	var w = single_zoom_width * (z + cumulative/maxCumulative);
	if (w > $("#zoomBar").width()) {
		w = $("#zoomBar").width();
	} else if (w < 0) {
		w = 0;
	}
	$("#zoomProgress").width(w);
}

function resetFingerZoom () {
	zoomFingers_searchFingersFlag = true;
	zoomFingers_framesPassed = 0;
	zoomFingers_cumulative = -1;
	// zoomFingers_enableZoomOut = true;
	// zoomFingers_enableZoomIn = true;
	$("#zoomBar, #zoomIn, #zoomOut, #zoomLimit").hide();
}

// checks thet there are at least 2 hands with one fingers each
// and in that case returns the left and the right hand
function checkZooomFingers (hands) {
	var toReturn;
	// h contains only hands with 2 fingers
	var h = [];

	for (var i = 0; i < hands.length; i++)
		if (hands[i].fingers.length>0)
			h[i] = hands[i];


	if (h.length < 2)
		toReturn = {valid: false};
	else
		toReturn = {valid: true, left: getLeftMostHand(h), right: getRightMostHand(h)};

	return toReturn;
}

function checkHandsForPinching (hands, fingers) {

	var toReturn = {valid:false};
	var l, r;

	if (hands.length == 2) {
		l = getLeftMostHand(hands);
		r = getRightMostHand(hands);
		if (Math.abs(l.palmNormal[0]) > 0.65 &&
			Math.abs(r.palmNormal[0]) > 0.65) {
			toReturn= {valid: true, left: l, right: r};
		}
	}

	else if (hands.length == 1 && fingers.length >=5 ) {
		//fingers detected, but not the hand

		var h1 = [];
		var h2 = [];

		var max_hole = {id: -1, value:0};

		fingers.sort(function(a,b) {
			return a.tipPosition[0]-b.tipPosition[0];
		});

		for (var i = 1; i < fingers.length; i++) {
			if (fingers[i].tipPosition[0]-fingers[i-1].tipPosition[0] > max_hole.value) {
				max_hole.value = fingers[i].tipPosition[0]-fingers[i-1].tipPosition[0];
				max_hole.id = i;
			}
		};

		for (var i = 0; i < fingers.length; i++) {
			if ( i < max_hole.id )
				h1.push(fingers[i]);
			else
				h2.push(fingers[i]);
		}

		var min_x_1 = fingers[0].tipPosition[0];
		var max_x_1 = fingers[max_hole.id-1].tipPosition[0];
		var min_x_2 = fingers[max_hole.id].tipPosition[0];
		var max_x_2 = fingers[fingers.length-1].tipPosition[0];

		if (Math.abs(max_x_1-min_x_1) < 40 && Math.abs(max_x_2-min_x_2) < 40 && max_hole.value > 100) {
			//if we found 2 groups of fingers that are close together

			var mean_pos_x_1 = 0;
			var mean_pos_x_2 = 0;
			var mean_pos_z_1 = 0;
			var mean_pos_z_2 = 0;
			var mean_vel_x_1 = 0;
			var mean_vel_x_2 = 0;

			for (var i = 0; i < h1.length; i++) {
				mean_pos_x_1 += h1[i].tipPosition[0];
				mean_pos_z_1 += h1[i].tipPosition[2];
				mean_vel_x_1 += h1[i].tipVelocity[0];
			}
			mean_pos_x_1 /= (i+1);
			mean_pos_z_1 /= (i+1) + 50;
			mean_vel_x_1 /= (i+1);

			for (var i = 0; i < h2.length; i++) {
				mean_pos_x_2 += h2[i].tipPosition[0];
				mean_pos_z_2 += h2[i].tipPosition[2];
				mean_vel_x_2 += h2[i].tipVelocity[0];
			}
			mean_pos_x_2 /= (i+1);
			mean_pos_z_2 /= (i+1) + 50;
			mean_vel_x_2 /= (i+1);

			console.log(mean_pos_x_1, mean_pos_x_2);

			l = {
				palmPosition: [mean_pos_x_1, -1, mean_pos_z_1],
				palmVelocity: [mean_vel_x_1, -1, -1]
			};
			r = {
				palmPosition: [mean_pos_x_2, -1, mean_pos_z_2],
				palmVelocity: [mean_vel_x_2, -1, -1]
			};

			toReturn= {valid: true, left: l, right: r};

		} else
			console.log("-------- "+Math.abs(max_x_1-min_x_1), Math.abs(max_x_2-min_x_2), max_hole.value);
	}

	return toReturn;
}

function resetHandsPinchZoom () {
	zoomHandPinch_searchFingersFlag = true;
	zoomHandPinch_framesPassed = 0;
	zoomHandPinch_cumulative = -1;
	zoomHandPinch_enableZoomOut = true;
	zoomHandPinch_enableZoomIn = true;
	$("#zoomBar, #zoomIn, #zoomOut, #zoomLimit").hide();
}