var alreadySwitched = false; //flag used to avoid to draw things more than once per switch
var switchHasAlreadyHappened = false;
var previousFramesToCheck = 20;
var ignoreLeap = true;


function searchForUnlockGesture (leap) {

	var frame = leap.frame();

	// Wrist gesture: from 5 to 0 fingers in a short time, with only one hand, in a centered position over X and Z
	if (frame.hands.length == 1 && frame.pointables.length <= 1 &&
		!switchHasAlreadyHappened) {

		for (var i=1; i<previousFramesToCheck; i++)
			if (leap.frame(i).pointables.length >= 4 && leap.frame(i).hands.length == 1) {

				ignoreLeap = !ignoreLeap;

				$("#instructions").html(currentPan.string + " // " + currentZoom.string);
				console.log(currentPan.string + " // " + currentZoom.string);

				alreadySwitched = false; // used when drawing feedback on screen

				switchHasAlreadyHappened = true;
				break;
			}
	}

	if (frame.hands.length == 1 && frame.pointables.length >= 4 && switchHasAlreadyHappened) {
		switchHasAlreadyHappened = false;
	}

}