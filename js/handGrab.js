var flag = false;

function zoomHandGrab (leap) {
	var frame = leap.frame();
	$("#target").show();

	var h = frame.hands; //hands array
	var f = frame.pointables; //fingers array

	if ((h.length == 1 || h.length == 2) && f.length >=0) {

		var hand = getPreferredHand(h);
		var t = 14;

		if (Math.abs(hand.palmPosition[1]-hand.sphereCenter[1])<t && !flag) {
			console.log("IN");
			flag = true;
		} else if (Math.abs(hand.palmPosition[1]-hand.sphereCenter[1])>=t && flag) {
			console.log("OUT");
			flag = false;
		}
	}
}