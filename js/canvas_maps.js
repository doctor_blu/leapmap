
function drawCanvas() {
	
		$("#map-mirror").hide();
	
		var tileInformations = getTileInformations();
	
		var dim = 256;
	
		var canvas = document.getElementById('map-mirror');
		canvas.width = tileInformations.x_no * dim;
		canvas.height = tileInformations.y_no * dim;
		
		$("#map-mirror").css("top", $("img[src='"+ tileInformations.src_array[0] +"']").parent().css("top"));
		$("#map-mirror").css("left", $("img[src='"+ tileInformations.src_array[0] +"']").parent().css("left"));

		var context = canvas.getContext('2d');
		
		var images = {};
		
		
		var x = 0;
		var y = 0;
		
	
		loadImages(tileInformations.src_array, function(images) {
	
			context.clearRect(0, 0, canvas.width, canvas.height);
	
			for (var i = 0; i < tileInformations.src_array.length; i++) {
				x = dim * (i % tileInformations.x_no);
				y = dim * (Math.floor(i / tileInformations.x_no));
	
				context.drawImage(images[i], x, y, dim, dim);
			}
		});
		
		$("#map-mirror").show();
		
		
	};
	
	function loadImages(sources, callback) {
		var images = {};
		var loadedImages = 0;
		var numImages = 0;
		
		// get num of sources
		for(var src in sources) {
		  numImages++;
		}
		
		for(var src in sources) {
		  images[src] = new Image();
		  images[src].onload = function() {
		    if(++loadedImages >= numImages) {
		      callback(images);
		    }
		  };
		  images[src].src = sources[src];
		}
	}
	
	function getTileInformations() {
		
		var array = new Array();
		
		var x_values = new Array();
		var y_values = new Array();
		
		$("#map-canvas img").each(function(index) {
			array.push($(this).attr("src"));
		});
	
		array.pop();
		array.pop();
	
		array.sort(function(a,b) {
			var a_x = parseInt(a.substring(a.indexOf("&x")+3, a.indexOf("&y")));
			var a_y = parseInt(a.substring(a.indexOf("&y")+3, a.indexOf("&z")));
			
			var b_x = parseInt(b.substring(b.indexOf("&x")+3, b.indexOf("&y")));
			var b_y = parseInt(b.substring(b.indexOf("&y")+3, b.indexOf("&z")));
			
			if (a_y == b_y)
				return a_x - b_x;
			else
				return a_y - b_y;
		});
		
		
		var previous_x;
		var how_many_y = 0;
		
		for (var i=0; i<array.length; i++) {
			if (previous_x != parseInt(array[i].substring(array[i].indexOf("&y")+3, array[i].indexOf("&z")))) {
				previous_x = parseInt(array[i].substring(array[i].indexOf("&y")+3, array[i].indexOf("&z")));
				how_many_y++;
			}			
		}
		
		var toReturn = {
			src_array: array,
			x_no: array.length/how_many_y,
			y_no: how_many_y
		}
		
		return toReturn;
		
	}